#!/usr/bin/python

import sys
import os

def declare_visitor(f, base_name):
    class_name = base_name + "Visitor";
    f.write("struct " + class_name + ";\n")
    return class_name

def define_visitor(f, base_name, types):
    class_name = base_name + "Visitor";
    f.write("struct " + class_name + "\n")
    f.write("{\n")
    f.write("    virtual ~" + class_name + "() = default;\n")
    f.write("\n")
    for c in types.iterkeys():
        c.strip();
        f.write("    virtual void visit(" + c + "&) = 0;\n")
    f.write("};\n")

def declare_type(f, class_name):
    f.write("struct " + class_name + ";\n")

def define_type(f, base_name, class_name, fields, visitor_class_name):
    f.write("struct " + class_name + " : " + base_name + "\n")
    f.write("{\n")

    # constructor
    f.write("    explicit " + class_name + "(\n")
    for index, field in enumerate(fields.split(",")):
        t, n = field.strip().split(" ")
        f.write("            " + t + " " + n)
        f.write(",\n" if index != (len(fields.split(",")) - 1) else ")\n")

    # member init list
    for index, field in enumerate(fields.split(",")):
        t, n = field.strip().split(" ")
        f.write("        " + (": " if index == 0 else ", "))
        f.write(n + "(std::move(" + n + "))" + "\n")

    f.write("    {\n")
    f.write("    }\n")
    f.write("\n")

    # visitor accept
    f.write("    void accept(" + visitor_class_name + "& visitor) override\n")
    f.write("    {\n")
    f.write("        visitor.visit(*this);\n")
    f.write("    }\n")
    f.write("\n")

    # data members
    for field in fields.split(","):
        t, n = field.strip().split(" ")
        f.write("    " + t + " " + n + ";\n")

    f.write("};\n")
    f.write("\n")

def define_ast(output_dir, includes, base_name, types):
    path = os.path.join(output_dir, base_name.lower() + ".hpp")
    with open(path, 'w') as f:
        f.write("// Automatically created file, do not edit!\n")
        f.write("// Copyright H. Ploner\n")
        f.write("\n")

        # include guard
        f.write("#pragma once\n")
        f.write("\n")

        # includes
        for incl in includes:
            f.write("#include \"" + incl + "\"\n")
        if len(includes) > 0:
            f.write("\n")
        f.write("#include <memory>\n")
        f.write("\n")

        # visitor declaration
        visitor_class_name = declare_visitor(f, base_name)
        f.write("\n")

        # type forward declarations
        for class_name in types.iterkeys():
            class_name.strip();
            declare_type(f, class_name);
        f.write("\n")

        # base class definition
        f.write("struct " + base_name + "\n")
        f.write("{\n")
        f.write("    virtual ~" + base_name + "() = default;\n")
        f.write("\n")
        f.write("    virtual void accept(" + visitor_class_name + "&) = 0;\n")
        f.write("};\n")
        f.write("\n")

        # visitor definition
        define_visitor(f, base_name, types)
        f.write("\n")

        # types definitions
        for class_name, fields in types.items():
            class_name.strip();
            fields.strip();
            define_type(f, base_name, class_name, fields, visitor_class_name)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage: " + os.path.basename(sys.argv[0]) + ": <output_dir>"
        sys.exit()

    output_dir = sys.argv[1]

    define_ast(
            output_dir,
            ["token.hpp", "value.hpp"],
            "Expr",
            {
                "Assignment" : "Token name, std::shared_ptr<Expr> value",
                "Binary": "std::shared_ptr<Expr> left, Token op, std::shared_ptr<Expr> right",
                "Grouping" : "std::shared_ptr<Expr> expression",
                "Literal": "Value value",
                "Unary" : "Token op, std::shared_ptr<Expr> right",
                "Variable" : "Token name"
            })

    define_ast(
            output_dir,
            ["expr.hpp"],
            "Stmt",
            {
                "Expression" : "std::shared_ptr<Expr> expression",
                "Print" : "std::shared_ptr<Expr> expression",
                "Var" : "Token name, std::shared_ptr<Expr> initializer"
            })

