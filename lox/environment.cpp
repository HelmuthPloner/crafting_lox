#include "environment.hpp"

#include "runtime_error.hpp"

Environment::Environment()
{
}

void Environment::define(const std::string& name, const Value& value)
{
    values.emplace(name, value);
}

Value& Environment::get(const Token& name)
{
    const auto match = values.find(name.lexeme);

    return
            match != values.end()
            ? match->second
            : throw RuntimeError{name, "Undefined variable '" + name.lexeme + "'."};
}

void Environment::assign(const Token& name, const Value& value)
{
    const auto match = values.find(name.lexeme);

    if (match != values.cend())
    {
        match->second = value;
    }
    else
    {
        throw RuntimeError{name, "Undefined value '" + name.lexeme + "'.'"};
    }
}
