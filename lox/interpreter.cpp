#include "interpreter.hpp"

#include "runtime_error.hpp"
#include "stringify.hpp"

#include <cassert>
#include <iostream>
#include <stdexcept>
#include <variant>

Interpreter::Interpreter()
{
}

void Interpreter::interpret(const std::vector<std::shared_ptr<Stmt>>& statements)
{
    m_stack.clear();

    for (const auto& stmt : statements)
    {
        execute(*stmt);
    }

    assert(m_stack.empty());
}

void Interpreter::visit(Expression& expr)
{
    evaluate(*expr.expression);

    // discard expression on TOS
    assert(!m_stack.empty());
    m_stack.pop_back();
}

void Interpreter::visit(Print& print)
{
    evaluate(*print.expression);
    assert(!m_stack.empty());
    std::cout << stringify(m_stack.back()) << std::endl;
    m_stack.pop_back();
}

void Interpreter::visit(Var& var)
{
    auto initializer = Value{};
    if (var.initializer)
    {
        evaluate(*var.initializer);
        assert(!m_stack.empty());
        initializer = m_stack.back();
        m_stack.pop_back();
    }

    environment.define(var.name.lexeme, initializer);
}

namespace
{
    template<class... Ts> struct Visitor : Ts... { using Ts::operator()...; };
    template<class... Ts> Visitor(Ts...) -> Visitor<Ts...>;

    bool isTruthy(const Value& val)
    {
        return std::visit(
                Visitor {
                    [](Nil) { return false; },
                    [](const bool b) { return b; },
                    [](const auto&) { return true; }
                },
                val);
    }

    struct Equality
    {
        bool operator()(Nil, Nil) const
        {
            return true;
        }

        template<typename T>
        bool operator()(const T& t1, const T& t2) const
        {
            return t1 == t2;
        }

        template<typename T, typename U>
        bool operator()(const T&, const U&) const
        {
            return false;
        }
    };

    bool isEqual(const Value& left, const Value& right)
    {
        return std::visit(Equality{}, left, right);
    }
}

namespace
{
    double toDouble(const Value& v, const Token& t)
    {
        if (const auto d = std::get_if<double>(&v))
        {
            return *d;
        }
        else
        {
            throw RuntimeError{t, "Operand must be a double"};
        }
    }

    std::pair<double, double> toDouble(const Value& v1, const Value& v2, const Token& t)
    {
        const auto d1 = std::get_if<double>(&v1);
        const auto d2 = std::get_if<double>(&v2);

        if (!d1)
        {
            throw RuntimeError{t, "number expected for left-hand side operator"};
        }

        if (!d2)
        {
            throw RuntimeError{t, "number expected for right-hand side operator"};
        }

        return { *d1, *d2 };
    }
}

void Interpreter::visit(Assignment& assignment)
{
    evaluate(*assignment.value);
    assert(!m_stack.empty());
    const auto value = m_stack.back();
    environment.assign(assignment.name, value);
}

void Interpreter::visit(Unary& unary)
{
    evaluate(*unary.right);

    auto& right = m_stack.at(m_stack.size() - 1u);

    switch (unary.op.type)
    {
    case TokenType::MINUS:

        {
            const auto d = toDouble(right, unary.op);
            right = -d;
        }
        break;
    case TokenType::BANG:
        right = !isTruthy(right);
        break;
    default:
        ;
    }
}

void Interpreter::visit(Binary& binary)
{
    evaluate(*binary.left);
    evaluate(*binary.right);

    const auto right = m_stack.at(m_stack.size() - 1u);
    m_stack.pop_back();

    const auto left = m_stack.at(m_stack.size() - 1u);
    m_stack.pop_back();

    switch (binary.op.type)
    {
    case TokenType::PLUS:
        if (std::holds_alternative<double>(left) && std::holds_alternative<double>(right))
        {
            m_stack.emplace_back(std::get<double>(left) + std::get<double>(right));
        }
        else if (std::holds_alternative<std::string>(left) && std::holds_alternative<std::string>(right))
        {
            m_stack.emplace_back(std::get<std::string>(left) + std::get<std::string>(right));
        }
        else
        {
            throw RuntimeError{binary.op, "two doubles or two strings expected"};
        }
        break;

    case TokenType::MINUS:
        {
            const auto ops = toDouble(left, right, binary.op);
            m_stack.emplace_back(ops.first - ops.second);
        }
        break;

    case TokenType::STAR:
        {
            const auto ops = toDouble(left, right, binary.op);
            m_stack.emplace_back(ops.first * ops.second);
        }
        break;

    case TokenType::SLASH:
        {
            const auto ops = toDouble(left, right, binary.op);
            m_stack.emplace_back(ops.first / ops.second);
        }
        break;

    case TokenType::GREATER:
        {
            const auto ops = toDouble(left, right, binary.op);
            m_stack.emplace_back(ops.first < ops.second);
        }
        break;

    case TokenType::GREATER_EQUAL:
        {
            const auto ops = toDouble(left, right, binary.op);
            m_stack.emplace_back(ops.first <= ops.second);
        }
        break;

    case TokenType::LESS:
        {
            const auto ops = toDouble(left, right, binary.op);
            m_stack.emplace_back(ops.first > ops.second);
        }
        break;

    case TokenType::LESS_EQUAL:
        {
            const auto ops = toDouble(left, right, binary.op);
            m_stack.emplace_back(ops.first >= ops.second);
        }
        break;

    case TokenType::EQUAL_EQUAL:
        m_stack.emplace_back(isEqual(left, right));
        break;

    case TokenType::BANG_EQUAL:
        m_stack.emplace_back(!isEqual(left, right));
        break;

    default:
        ;
    }
}

void Interpreter::visit(Literal& literal)
{
    m_stack.emplace_back(literal.value);
}

void Interpreter::visit(Grouping& group)
{
    evaluate(*group.expression);
}

void Interpreter::visit(Variable& var)
{
    m_stack.emplace_back(environment.get(var.name));
}

void Interpreter::execute(Stmt& stmt)
{
    stmt.accept(*this);
}

void Interpreter::evaluate(Expr& expr)
{
    expr.accept(*this);
}
