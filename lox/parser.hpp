#pragma once

#include "expr.hpp"
#include "stmt.hpp"
#include "token.hpp"

#include <stdexcept>
#include <vector>

class Parser
{
public:
    struct Error : std::runtime_error
    {
        using std::runtime_error::runtime_error;
    };

    Parser(std::vector<Token> tokens);
    std::vector<std::shared_ptr<Stmt>> parse();

private:
    std::shared_ptr<Stmt> declaration();
    std::shared_ptr<Stmt> varDeclaration();

    std::shared_ptr<Stmt> statement();
    std::shared_ptr<Stmt> printStatement();
    std::shared_ptr<Stmt> expressionStatement();

    std::shared_ptr<Expr> expression();
    std::shared_ptr<Expr> assignment();
    std::shared_ptr<Expr> equality();
    std::shared_ptr<Expr> comparison();
    std::shared_ptr<Expr> addition();
    std::shared_ptr<Expr> multiplication();
    std::shared_ptr<Expr> unary();
    std::shared_ptr<Expr> primary();

    bool isAtEnd() const;

    Token advance();
    Token previous();

    bool match(const std::vector<TokenType>& types);
    bool check(TokenType type);
    Token peek();
    Token consume(TokenType type, const std::string& message);

    Error error(Token token, const std::string& message);
    void synchronize();

    std::vector<Token> tokens;
    std::vector<Token>::const_iterator current;
};
