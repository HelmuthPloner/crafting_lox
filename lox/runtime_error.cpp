#include "runtime_error.hpp"

RuntimeError::RuntimeError(Token t, const std::string& m)
    : std::runtime_error{m}
    , token(std::move(t))
{
}
