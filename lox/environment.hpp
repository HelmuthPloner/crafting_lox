#pragma once

#include "token.hpp"
#include "value.hpp"

#include <map>
#include <string>

class Environment
{
public:
    Environment();

    void define(const std::string& name, const Value& value);
    Value& get(const Token& name);
    void assign(const Token& name, const Value& value);

private:
    std::map<std::string, Value> values;
};
