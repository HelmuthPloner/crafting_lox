// Automatically created file, do not edit!
// Copyright H. Ploner

#pragma once

#include "token.hpp"
#include "value.hpp"

#include <memory>

struct ExprVisitor;

struct Binary;
struct Assignment;
struct Unary;
struct Literal;
struct Variable;
struct Grouping;

struct Expr
{
    virtual ~Expr() = default;

    virtual void accept(ExprVisitor&) = 0;
};

struct ExprVisitor
{
    virtual ~ExprVisitor() = default;

    virtual void visit(Binary&) = 0;
    virtual void visit(Assignment&) = 0;
    virtual void visit(Unary&) = 0;
    virtual void visit(Literal&) = 0;
    virtual void visit(Variable&) = 0;
    virtual void visit(Grouping&) = 0;
};

struct Binary : Expr
{
    explicit Binary(
            std::shared_ptr<Expr> left,
            Token op,
            std::shared_ptr<Expr> right)
        : left(std::move(left))
        , op(std::move(op))
        , right(std::move(right))
    {
    }

    void accept(ExprVisitor& visitor) override
    {
        visitor.visit(*this);
    }

    std::shared_ptr<Expr> left;
    Token op;
    std::shared_ptr<Expr> right;
};

struct Assignment : Expr
{
    explicit Assignment(
            Token name,
            std::shared_ptr<Expr> value)
        : name(std::move(name))
        , value(std::move(value))
    {
    }

    void accept(ExprVisitor& visitor) override
    {
        visitor.visit(*this);
    }

    Token name;
    std::shared_ptr<Expr> value;
};

struct Unary : Expr
{
    explicit Unary(
            Token op,
            std::shared_ptr<Expr> right)
        : op(std::move(op))
        , right(std::move(right))
    {
    }

    void accept(ExprVisitor& visitor) override
    {
        visitor.visit(*this);
    }

    Token op;
    std::shared_ptr<Expr> right;
};

struct Literal : Expr
{
    explicit Literal(
            Value value)
        : value(std::move(value))
    {
    }

    void accept(ExprVisitor& visitor) override
    {
        visitor.visit(*this);
    }

    Value value;
};

struct Variable : Expr
{
    explicit Variable(
            Token name)
        : name(std::move(name))
    {
    }

    void accept(ExprVisitor& visitor) override
    {
        visitor.visit(*this);
    }

    Token name;
};

struct Grouping : Expr
{
    explicit Grouping(
            std::shared_ptr<Expr> expression)
        : expression(std::move(expression))
    {
    }

    void accept(ExprVisitor& visitor) override
    {
        visitor.visit(*this);
    }

    std::shared_ptr<Expr> expression;
};

