#pragma once

#include "token.hpp"

#include <string>

class Lox
{
public:
    static void runFile(const std::string& file_name);
    static void runPrompt();

    static void report(int line, const std::string& where, const std::string& message);
    static void error(Token token, const std::string& message);
    static bool had_error;
private:
    static void run(const std::string& bytes);
};
