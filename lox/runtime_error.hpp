#pragma once

#include "token.hpp"

#include <stdexcept>
#include <string>

struct RuntimeError : std::runtime_error
{
    RuntimeError(Token token, const std::string& message);

    Token token;
};
