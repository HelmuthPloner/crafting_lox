#include "interpreter.hpp"
#include "lox.hpp"
#include "runtime_error.hpp"

#include <cstdlib>
#include <iostream>

int main(int argc, char* argv[])
{
    if (argc > 2)
    {
        std::cout << "Usage: " << argv[0] << " [script]" << std::endl;
        return -1;
    }
    else if (argc == 2)
    {
        try
        {
            Lox::runFile(argv[1]);
            if (Lox::had_error)
            {
                std::exit(58);
            }
        }
        catch (const RuntimeError&)
        {
            std::exit(59);
        }
    }
    else
    {
        Lox::runPrompt();
    }
}
