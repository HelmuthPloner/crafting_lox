#include "scanner.hpp"

#include "error.hpp"

#include <iterator>
#include <map>

Scanner::Scanner(std::string bytes)
    : source(std::move(bytes))
    , start(source.cbegin())
    , current(start)
    , line{1u}
{
}

std::vector<Token> Scanner::scanTokens()
{
    while (!atEnd())
    {
        start = current;
        scanToken();
    }

    tokens.emplace_back(TokenType::EOF_, "", Value(), line);

    return tokens;
}

bool Scanner::atEnd() const
{
    return current == source.cend();
}

bool Scanner::match(std::string::value_type expected)
{
    if (atEnd()) return false;
    if (*current != expected) return false;

    ++current;
    return true;
}

std::string::value_type Scanner::peek() const
{
    return atEnd() ? '\n' : *current;
}

std::string::value_type Scanner::peekNext() const
{
    return std::distance(current, source.cend()) >= 1 ? *(current + 1) : '\n';
}

namespace
{
    bool isDigit(std::string::value_type ch)
    {
        return '0' <= ch && ch <= '9';
    }

    bool isAlpha(std::string::value_type ch)
    {
        return ('A' <= ch && ch <= 'Z') || ('a' <= ch && ch <= 'z') || ch == '_';
    }

    bool isAlphaNumeric(std::string::value_type ch)
    {
        return isDigit(ch) || isAlpha(ch);
    }
}

void Scanner::scanToken()
{
    const auto ch = advance();

    switch (ch)
    {
    case '(': addToken(LEFT_PAREN); break;
    case ')': addToken(RIGHT_PAREN); break;
    case '{': addToken(LEFT_BRACE); break;
    case '}': addToken(RIGHT_BRACE); break;
    case ',': addToken(COMMA); break;
    case '.': addToken(DOT); break;
    case '-': addToken(MINUS); break;
    case '+': addToken(PLUS); break;
    case ';': addToken(SEMICOLON); break;
    case '*': addToken(STAR); break;

    case '/':
        if (match('/'))
        {
            while (peek() != '\n' && !atEnd())
            {
                advance();
            }
        }
        else
        {
            addToken(SLASH);
        }
        break;

    // One or two character tokens.
    case '!': addToken(match('=') ? BANG_EQUAL : BANG); break;
    case '=': addToken(match('=') ? EQUAL_EQUAL : EQUAL); break;
    case '>': addToken(match('=') ? GREATER_EQUAL : GREATER); break;
    case '<': addToken(match('=') ? LESS_EQUAL : LESS); break;

    case ' ': case '\r': case '\t':
        // ignore whitespace
        break;

    case '\n':
        ++line;
        break;

    case '\"':
        string();
        break;

    default:
        if (isDigit(ch))
        {
            number();
        }
        else if (isAlpha(ch))
        {
            identifier();
        }
        else
        {
            error(line, std::string("Unexpected character \'") + ch + '\'');
        }
    }
}

void Scanner::string()
{
    while (peek() != '\"' && !atEnd())
    {
        if (peek() == '\n') ++line;
        advance();
    }

    if (atEnd())
    {
        error(line, "Unterminated string.");
        return;
    }

    // closing "
    advance();

    addToken(STRING, std::string(start + 1, current - 1));
}

void Scanner::number()
{
    while (isDigit(peek())) advance();

    if (peek() == '.' && isDigit(peekNext()))
    {
        advance();
        while (isDigit(peek())) advance();
    }

    addToken(NUMBER, std::stod(std::string(start, current)));
}

void Scanner::identifier()
{
    while (isAlphaNumeric(peek())) advance();

    const auto text = std::string(start, current);

    static const std::map<std::string, TokenType> keywords {
        { "and", AND },
        { "class", CLASS },
        { "else", ELSE },
        { "false", FALSE },
        { "fun", FUN },
        { "for", FOR },
        { "if", IF },
        { "nil", NIL },
        { "or", OR },
        { "print", PRINT },
        { "return", RETURN },
        { "super", SUPER },
        { "this", THIS },
        { "true", TRUE },
        { "var", VAR },
        { "while", WHILE }
    };

    const auto match = keywords.find(text);

    addToken(match == keywords.cend() ? IDENTIFIER : match->second, text);
}

std::string::value_type Scanner::advance()
{
    return *current++;
}

void Scanner::addToken(const TokenType token_type)
{
    addToken(token_type, Value());
}

void Scanner::addToken(const TokenType token_type, Value object)
{
    tokens.emplace_back(token_type, std::string(start, current), std::move(object), line);
}
