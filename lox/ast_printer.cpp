#include "ast_printer.hpp"

#include <variant>

std::string AstPrinter::print(Expr& expr)
{
    expr.accept(*this);
    return rep;
}

void AstPrinter::visit(Unary& unary)
{
    parenthesize(unary.op.lexeme, { unary.right });
}

void AstPrinter::visit(Binary& binary)
{
    parenthesize(binary.op.lexeme, { binary.left, binary.right });
}

namespace
{
    struct StringConverter
    {
        std::string operator()(const std::string& s) const
        {
            return s;
        }

        std::string operator()(const double d) const
        {
            return std::to_string(d);
        }

        std::string operator()(const bool b) const
        {
            return b ? "true" : "false";
        }

        std::string operator()(Nil) const
        {
            return "nil";
        }
    };
}

void AstPrinter::visit(Literal& literal)
{
    rep += std::visit(StringConverter{}, literal.value);
}

void AstPrinter::visit(Grouping& grouping)
{
    parenthesize("group", { grouping.expression });
}

void AstPrinter::parenthesize(const std::string& name, const std::vector<std::shared_ptr<Expr>>& exprs)
{
    rep += '(';
    rep += name;
    for (auto& expr : exprs)
    {
        rep += " ";
        expr->accept(*this);
    }
    rep += ')';
}
