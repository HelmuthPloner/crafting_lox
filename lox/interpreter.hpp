#pragma once

#include "environment.hpp"
#include "expr.hpp"
#include "stmt.hpp"
#include "value.hpp"

#include <memory>
#include <vector>

class Interpreter : public ExprVisitor, public StmtVisitor
{
public:
    Interpreter();

    void interpret(const std::vector<std::shared_ptr<Stmt>>& statements);

    void visit(Expression&) override;
    void visit(Print&) override;
    void visit(Var&) override;

    void visit(Assignment&) override;
    void visit(Unary&) override;
    void visit(Binary&) override;
    void visit(Literal&) override;
    void visit(Grouping&) override;
    void visit(Variable&) override;

private:
    void execute(Stmt&);
    void evaluate(Expr&);

    Environment environment;
    std::vector<Value> m_stack;
};
