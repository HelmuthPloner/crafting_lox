#pragma once

#include "expr.hpp"

#include <memory>
#include <string>
#include <vector>

class AstPrinter : public ExprVisitor
{
public:
    std::string print(Expr& expr);

    void visit(Unary&) override;
    void visit(Binary&) override;
    void visit(Literal&) override;
    void visit(Grouping&) override;

private:
    void parenthesize(const std::string& name, const std::vector<std::shared_ptr<Expr>>& exprs);

    std::string rep;
};
