#include "lox.hpp"

#include "interpreter.hpp"
#include "parser.hpp"
#include "runtime_error.hpp"
#include "scanner.hpp"
#include "token.hpp"

#include <fstream>
#include <iostream>
#include <sstream>

bool Lox::had_error;

void Lox::runFile(const std::string& file_name)
{
    std::ifstream in(file_name);
    std::ostringstream content;
    content << in.rdbuf();
    Lox::run(content.str());
}

void Lox::runPrompt()
{
    std::cout << "> ";
    for (std::string input; getline(std::cin, input); std::cout << "> ")
    {
        try
        {
            Lox::had_error = false;
            Lox::run(input);
        }
        catch (...)
        {
        }
    }
}

void Lox::run(const std::string& bytes)
{
    Scanner scanner(bytes);
    const auto all_tokens = scanner.scanTokens();

    Parser parser{all_tokens};
    const auto ast = parser.parse();

    if (Lox::had_error) return;

    try
    {
        static Interpreter interpreter;
        interpreter.interpret(ast);
    }
    catch (const RuntimeError& e)
    {
        Lox::error(e.token, e.what());
        throw;
    }
}

void Lox::report(int line, const std::string& where, const std::string& message)
{
    std::cerr << "[line " << line << "] Error" << where << ": " << message << std::endl;
    had_error = true;
}

void Lox::error(Token token, const std::string &message)
{
    if (token.type == TokenType::EOF_)
    {
        report(token.line, " at end", message);
    }
    else
    {
        report(token.line, " at '" + token.lexeme + "'", message);
    }
}

