#include "stringify.hpp"

#include <regex>
#include <variant>

namespace
{
    struct Printer
    {
        std::string operator()(const double v) const
        {
            const auto s = std::to_string(v);
            std::regex trailing_zeros(R"(\.0+)");
            return std::regex_replace(s, trailing_zeros, "");
        }

        std::string operator()(const std::string& s) const
        {
            return s;
        }

        std::string operator()(Nil) const
        {
            return "nil";
        }

        std::string operator()(const bool b) const
        {
            return b ? "true" : "false";
        }
    };
}

std::string stringify(const Value& value)
{
    return std::visit(Printer{}, value);
}
