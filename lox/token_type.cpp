#include "token_type.hpp"

#include <ostream>

std::string toString(TokenType t)
{
    return std::to_string(t);
}

std::ostream& operator<<(std::ostream& os, TokenType t)
{
    return os << toString(t);
}
