#include "token.hpp"

Token::Token(TokenType t, std::string le, Value o, int li)
    : type{t}
    , lexeme(std::move(le))
    , object(std::move(o))
    , line{li}
{
}

std::string Token::toString() const
{
    return ::toString(type) + ' ' + lexeme + ' ';
}
