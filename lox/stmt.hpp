// Automatically created file, do not edit!
// Copyright H. Ploner

#pragma once

#include "expr.hpp"

#include <memory>

struct StmtVisitor;

struct Print;
struct Var;
struct Expression;

struct Stmt
{
    virtual ~Stmt() = default;

    virtual void accept(StmtVisitor&) = 0;
};

struct StmtVisitor
{
    virtual ~StmtVisitor() = default;

    virtual void visit(Print&) = 0;
    virtual void visit(Var&) = 0;
    virtual void visit(Expression&) = 0;
};

struct Print : Stmt
{
    explicit Print(
            std::shared_ptr<Expr> expression)
        : expression(std::move(expression))
    {
    }

    void accept(StmtVisitor& visitor) override
    {
        visitor.visit(*this);
    }

    std::shared_ptr<Expr> expression;
};

struct Var : Stmt
{
    explicit Var(
            Token name,
            std::shared_ptr<Expr> initializer)
        : name(std::move(name))
        , initializer(std::move(initializer))
    {
    }

    void accept(StmtVisitor& visitor) override
    {
        visitor.visit(*this);
    }

    Token name;
    std::shared_ptr<Expr> initializer;
};

struct Expression : Stmt
{
    explicit Expression(
            std::shared_ptr<Expr> expression)
        : expression(std::move(expression))
    {
    }

    void accept(StmtVisitor& visitor) override
    {
        visitor.visit(*this);
    }

    std::shared_ptr<Expr> expression;
};

