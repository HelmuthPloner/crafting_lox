#pragma once

#include "token_type.hpp"
#include "value.hpp"

#include <string>

class Token
{
public:
    Token(TokenType t, std::string lexeme, Value object, int line);

    std::string toString() const;

    TokenType type;
    std::string lexeme;
    Value object;
    int line;
};
