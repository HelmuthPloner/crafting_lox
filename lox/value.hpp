#pragma once

#include <string>
#include <variant>

struct Nil { };

using Value = std::variant<Nil, double, std::string, bool>;
