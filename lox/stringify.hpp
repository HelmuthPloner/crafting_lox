#pragma once

#include "expr.hpp"

#include <string>

std::string stringify(const Value& value);
