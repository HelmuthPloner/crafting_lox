#pragma once

#include "token.hpp"
#include "value.hpp"

#include <cstddef>
#include <string>
#include <vector>

class Scanner
{
public:
    explicit Scanner(std::string bytes);
    std::vector<Token> scanTokens();

private:
    bool atEnd() const;
    bool match(std::string::value_type expected);
    std::string::value_type peek() const;
    std::string::value_type peekNext() const;
    void scanToken();
    std::string::value_type advance();

    void string();
    void number();
    void identifier();

    void addToken(TokenType);
    void addToken(TokenType, Value object);

    std::string source;
    std::vector<Token> tokens;

    std::string::const_iterator start;
    std::string::const_iterator current;
    std::size_t line;
};
