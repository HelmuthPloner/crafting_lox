#include "error.hpp"

#include "lox.hpp"

void error(int line, const std::string& message)
{
    Lox::report(line, "", message);
}
