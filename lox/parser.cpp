#include "parser.hpp"

#include "error.hpp"
#include "lox.hpp"
#include "value.hpp"

#include <algorithm>

Parser::Parser(std::vector<Token> t)
    : tokens(std::move(t))
    , current(tokens.begin())
{
}

std::vector<std::shared_ptr<Stmt>> Parser::parse()
{
    try
    {
        std::vector<std::shared_ptr<Stmt>> statements;

        while (!isAtEnd())
        {
            try
            {
                statements.emplace_back(declaration());
            }
            catch (...)
            {
                synchronize();
            }
        }

        return statements;
    }
    catch (...)
    {
        return { };
    }
}

std::shared_ptr<Stmt> Parser::declaration()
{
    if (match({VAR}))
    {
        return varDeclaration();
    }
    else
    {
        return statement();
    }
}

std::shared_ptr<Stmt> Parser::varDeclaration()
{
    const auto name = consume(IDENTIFIER, "Expect variable name");

    const auto initializer = match({EQUAL}) ? expression() : std::shared_ptr<Expr>(nullptr);

    consume(SEMICOLON, "Expect ';' after variable declaration");

    return std::make_shared<Var>(name, initializer);
}

std::shared_ptr<Stmt> Parser::statement()
{
    if (match({PRINT}))
    {
        return printStatement();
    }
    else
    {
        return expressionStatement();
    }
}

std::shared_ptr<Stmt> Parser::printStatement()
{
    const auto value = expression();
    consume(SEMICOLON, "Expected ';' after value.");
    return std::make_shared<Print>(value);
}

std::shared_ptr<Stmt> Parser::expressionStatement()
{
    const auto expr = expression();
    consume(SEMICOLON, "Expected ';' after expr.");
    return std::make_shared<Expression>(expr);
}

std::shared_ptr<Expr> Parser::expression()
{
    return assignment();
}

std::shared_ptr<Expr> Parser::assignment()
{
    const auto expr = equality();

    if (match({EQUAL}))
    {
        Token equals = previous();
        const auto value = assignment();

        if (const auto var = std::dynamic_pointer_cast<Variable>(expr))
        {
            const auto name = var->name;
            return std::make_shared<Assignment>(name, value);
        }
        else
        {
            error(equals, "Invalid assignment target.");
        }
    }

    return expr;
}

std::shared_ptr<Expr> Parser::equality()
{
    auto expr = comparison();

    while (match({BANG_EQUAL, EQUAL_EQUAL}))
    {
        auto op = previous();
        auto right = comparison();
        expr = std::make_shared<Binary>(expr, op, right);
    }

    return expr;
}

std::shared_ptr<Expr> Parser::comparison()
{
    auto expr = addition();

    while (match({GREATER, GREATER_EQUAL, LESS, LESS_EQUAL}))
    {
        auto op = previous();
        auto right = addition();
        expr = std::make_shared<Binary>(expr, op, right);
    }

    return expr;
}

std::shared_ptr<Expr> Parser::addition()
{
    auto expr = multiplication();

    while (match({MINUS, PLUS}))
    {
        auto op = previous();
        auto right = multiplication();
        expr = std::make_shared<Binary>(expr, op, right);
    }

    return expr;
}

std::shared_ptr<Expr> Parser::multiplication()
{
    auto expr = unary();

    while (match({SLASH, STAR}))
    {
        auto op = previous();
        auto right = unary();
        expr = std::make_shared<Binary>(expr, op, right);
    }

    return expr;
}

std::shared_ptr<Expr> Parser::unary()
{
    if (match({BANG, MINUS}))
    {
        auto op = previous();
        auto right = unary();
        return std::make_shared<Unary>(op, right);
    }
    else
    {
        return primary();
    }
}

std::shared_ptr<Expr> Parser::primary()
{
    if (match({FALSE})) return std::make_shared<Literal>(false);
    if (match({TRUE})) return std::make_shared<Literal>(true);
    if (match({NIL})) return std::make_shared<Literal>(Value());

    if (match({NUMBER, STRING}))
    {
        return std::make_shared<Literal>(previous().object);
    }

    if (match({LEFT_PAREN}))
    {
        auto expr = expression();
        consume(RIGHT_PAREN, "Expected ')' after expression");
        return expr;
    }

    if (match({IDENTIFIER}))
    {
        return std::make_shared<Variable>(previous());
    }

    throw error(peek(), "Expect expression");
}

bool Parser::isAtEnd() const
{
    return current->type == EOF_;
}

Token Parser::advance()
{
    return !isAtEnd() ? *current++ : previous();
}

Token Parser::previous()
{
    return *(current - 1);
}

bool Parser::match(const std::vector<TokenType>& types)
{
    const auto match =
            std::find_if(
                types.cbegin(), types.cend(),
                [this](const TokenType type) { return check(type); });

    if (match != types.cend())
    {
        advance();
        return true;
    }
    else
    {
        return false;
    }
}

bool Parser::check(TokenType type)
{
    return !isAtEnd() && peek().type == type;
}

Token Parser::peek()
{
    return *current;
}

Token Parser::consume(TokenType type, const std::string& message)
{
    if (check(type))
    {
        return advance();
    }
    else
    {
        throw error(peek(), message);
    }
}

Parser::Error Parser::error(Token token, const std::string& message)
{
    Lox::error(token, message);
    return Error{""};
}

void Parser::synchronize()
{
    advance();

    while (!isAtEnd())
    {
        if (previous().type == SEMICOLON) return;

        switch (peek().type)
        {
        case CLASS:
        case FUN:
        case VAR:
        case FOR:
        case IF:
        case WHILE:
        case PRINT:
        case RETURN:
            return;

        default:
            advance();
        }
    }
}
